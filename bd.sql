DROP TABLE IF EXISTS user;
CREATE TABLE user (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT comment 'primary key',
    username VARCHAR (225) NOT NULL,
    password VARCHAR (225) NOT NULL,
    email VARCHAR (225) NOT NULL UNIQUE,
    role VARCHAR (225)
    )default charset utf8 comment '';


DROP TABLE IF EXISTS post;
CREATE TABLE post(  
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    title VARCHAR (225) NOT NULL,
    content VARCHAR (225) NOT NULL,
    image VARCHAR (255),
    commentaire VARCHAR (255),
    userId int(11) DEFAULT NULL,
    KEY userId_fk (userId),
    CONSTRAINT userId_fk FOREIGN KEY (userId) REFERENCES user (id) ON DELETE SET NULL ON UPDATE SET NULL
    ) default charset utf8 comment '';