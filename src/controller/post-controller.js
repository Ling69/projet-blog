import { Router } from "express";
import passport from "passport";
import { Post } from "../entity/Post";
import { PostRepository } from "../repository/PostRepository";
import { uploader } from "../uploader";

export const postController = Router();

postController.get('/', async(req, res) => {
    try {
        const post = await PostRepository.findAll();
        res.json(post);
       
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

postController.get('/:id', async (req, res) => {
    try {
        let article = await PostRepository.findById(req.params.id);
        res.json(article);

    } catch (error) {
        console.log(error);
        res.status(404).end;
    }

})


postController.get('/:title', async(req, res) => {
    try {
        const title = await PostRepository.findByTitle(req.params.title);
        res.json(title);
       
    } catch (error) {
        console.log(error);
        res.status(404).json(error)
    }
})

postController.get('/author/:userId', async (req, res) => {
    try {
        const userId = await PostRepository.findByUserId(req.params.userId);
        res.json(userId);
        
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})


postController.post('/', uploader.single('picture'), passport.authenticate('jwt', {session: false}), async(req, res) => {


    try {
        let newPost = new Post();
        Object.assign(newPost, req.body); 
        newPost.image = '/uploads/'+req.file.filename;
       
        //assigner à newPost.userId l'id du user connecté (req.user.id)

        newPost.userId = req.user.id;
       
        // console.log(newPost);
        await PostRepository.addPost(newPost);

        res.status(201).json(newPost);      
        
        
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
        
    }
})


postController.put('/', async (req, res) => {
    try {
        await PostRepository.updatePost(req.body);
        res.json(req.body);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
postController.delete('/:id', passport.authenticate('jwt', {session: false}), async (req, res) => {
    try {
        await PostRepository.deletePost(req.params.id);
        res.status(204).end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})
