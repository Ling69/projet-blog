import { Router } from "express";
import { User } from "../entity/User";
import { UserRepository } from "../repository/UserRepository";
import bcrypt from 'bcrypt';
import { gererateToken, protect } from "../utils/token";
import passport from "passport";

export const userController = Router();


userController.get('/', protect(), async (req, res) => {

    try {

        const data = await UserRepository.findAll();
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
    }
})

userController.post('/', async (req, res) => {

    try {

        const newUser = new User();
        Object.assign(newUser, req.body);

        const existes = await UserRepository.findByEmail(newUser.email);
        if(existes) {
            res.status(400).json({ error: 'email has been taken'});
            return;
        }

        newUser.role = 'user';
        newUser.password = await bcrypt.hash(newUser.password,12);

        await UserRepository.addUser(newUser);
        res.status(201).json(newUser);

        
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});

userController.post('/login', async(req, res) => {
    // console.log(req.body);
    try {
        const user = await UserRepository.findByEmail(req.body.email);
        if(user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if(samePassword) {
                res.json({
                    user,
                    token: gererateToken({
                        email: user.email,
                        id: user.id
                    })
                });
                return;
            }
        }
        res.status(401).json({
            error: 'wrong email and/or password'
        });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }  
});

userController.get('/account', passport.authenticate('jwt', {session:false}), async (req, res) => {
    res.json(req.user);
} )









