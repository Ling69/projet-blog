
import fs from 'fs';
import jwt  from 'jsonwebtoken';
import passport from 'passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserRepository } from '../repository/UserRepository';



// const privateKey = fs.readFileSync('config/id_rsa');
// const publicKey = fs.readFileSync('config/id_rsa.pub');

/**
 * Fonction qui génère un JWT en se basant sur la clef privée.
 * @param {object} payload payload le body qui sera mis dans le token
 * @returns le jwt
 */
export function gererateToken(payload, expire = 60 * 60) {
    const token= jwt.sign(payload, process.env.JWT_SECRET, {expiresIn: expire });
    return token;
}

/**
 * Fonction qui configure la stratégie JWT de passport, il va chercher le token dans les headers de la 
 * requête en mode {"authorization":"Bearer leToken"}
 */
export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT_SECRET
    }, async(payload, done) => {
        //findByEmail pour récupérer le User et pouvoir y donner accès dans les routes protégées
        try {
            const user = await UserRepository.findByEmail(payload.email);
            if (user) {
                return done(null, user);
            }

            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error,false);
            
        }

    }))
}

/**
 * Fonction permettant de protéger une route, optionnelement selon le role du user
 * @param {string[]} role role un array de role autorisés à accéder à la route (si any, alors n'importe quel role peut y accéder)
 * @returns un array de middleware, donc celui du jwt
 */

export function protect(role = ['any']) {

    return [
        passport.authenticate('jwt', {session: false}),
        (req,res,next) => {
            if(role.includes('any') || role.includes(req.user.role)) {
                next()
            } else {
                res.status(403).json({error: 'Access denied'});
            }
        }
    ]
}
