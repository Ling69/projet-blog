
import { User } from "../entity/User";
import { connection } from "./connection";

export class UserRepository {

     /**
     * persists a user
     * @param {User} user user to persist
     */

    static async addUser(user) {
        const [rows] = await connection.execute ('INSERT INTO user (username,password,email,role) VALUES (?, ?, ?, ?)', [user.username, user.password, user.email, user.role]);
        user.id = rows.insertId;
    }

    /**
     * get all users from database
     * @returns {Promise<User[]};
     */
    static async findAll(){
        const [rows] = await connection.execute('SELECT * FROM user');
        const users = [];
        for (const row of rows) {
            const user = new User(row.usename, row.password, row.email, row.role, row.id);
            users.push(user)
        }
        return users;

    }
    /**
     * find user by email from database
     * @param {String} email 
     * @returns {Promise<User} find user or nothing
     */
    static async findByEmail(email) {
        const [rows] = await connection.execute('SELECT * FROM user WHERE email=?', [email]);
        if (rows.length === 1) {
            return new User(rows[0].username, rows[0].password, rows[0].email, rows[0].role,rows[0].id)
        }
        return null;
    }
}