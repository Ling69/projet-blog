import { Post } from "../entity/Post";
import { connection } from "./connection";


export class PostRepository {

    /**
     * Method get all users from database
     * @returns {Promise<Post[]};
     */
     static async findAll(){
        const [rows] = await connection.execute('SELECT * FROM post');
        const posts = [];
        for (const row of rows) {
            const post = new Post (row.title, row.content, row.image, row.userId, row.author, row.released, row.commentaire, row.id);
            posts.push(post)
        }
        return posts;

    }

    /**
     * Method find by post id
     * @param {Number} id 
     * @returns The post id
     */
    static async findById(id) {
        let [rows] = await connection.execute('SELECT * FROM post WHERE ID=?', [id]);
        if (rows.length === 0) {
            return null;
        } else {
            return new Post(rows[0].title, rows[0].content, rows[0].image, rows[0].userId, rows[0].author, rows[0].released, rows[0].commentaire, rows[0].id);

        }

    }
    /**
     * Method find by post title
     * @param {String} title 
     * @returns The title
     */
    static async findByTitle(title) {
        const [rows] = await connection.execute('SELECT * FROM post WHERE title=?', [title]);
        if (rows.length === 1) {
            return new Post(rows[0].title, rows[0].content, rows[0].image, rows[0].userId, rows[0].author, rows[0].released, rows[0].commentaire, rows[0].id)
        }
        return null;
    }
    /**
     * userId with witch we can find the authors who publish their articles
     * @param {Number} userId 
     * @returns  userId
     */
    static async findByUserId (userId) {
       const [rows] = await connection.execute('SELECT * FROM post WHERE userId=?', [userId]);
        if (rows.length === 0) {
            return null;
        } else {
            return new Post(rows[0].title, rows[0].content, rows[0].image, rows[0].commentaire, rows[0].userId, row[0].author, row[0].released, rows[0].id);
        }
        
    }
  /**
   * Method add a post
   * @param {String} post add a new post
   */
    static async addPost(post) {
        const [rows] = await connection.execute('INSERT INTO post (title, content, image, userId, author, released, commentaire) VALUES (?, ?, ?, ?, ?, ?, ?)', [post.title, post.content, post.image, post.userId, post.author, post.released, post.commentaire]);
        post.id = rows.insertId
    }

    /**
     * Method update the post
     * @param {Post} updateData 
     */
    static async updatePost(updateData) {
        await connection.execute('UPDATE post SET title=?, content=?, image=?, userId=?, author=? released?, commentaire=?,  WHERE id=?', [updateData.title, updateData.content, updateData.image, updateData.userId, updateData.author, updateData.released, updateData.commentaire, updateData.id]);

    }
    /**
     * Method delete a post
     * @param {Post} post 
     */
    static async deletePost(post) {
        await connection.execute('DELETE FROM post WHERE id=?', [post]);

    }

    }

    
    

   

