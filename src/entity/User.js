
export class User {
    id;
    username;
    password;
    email;
    role;
    
    /**
     * 
     * @param {String} username 
     * @param {String} password 
     * @param {String} email 
     * @param {String} role 
     * @param {Number} id 
     */
    constructor(username, password, email, role = "user", id=null ) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = "user";
        this.id = id
    }

}

