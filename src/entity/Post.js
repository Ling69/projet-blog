
export class Post {
    id;
    title;
    content;
    image;
    userId;
    author;
    released;
    commentaire;
    // notes
    
       /**
     * 
     * @param {String} title 
     * @param {String} content 
     * @param {String} image 
     * @param {Number} userId 
     * @param {String} author 
     * @param {Number} released 
     * @param {String} commentaire 
     * @param {Number} id 
     */
    constructor (title, content, image, userId, author, released, commentaire, id=null) {
        this.title = title;
        this.content = content;
        this.image = image;
        this.userId = userId;
        this.author = author;
        this.released = released;
        this.commentaire = commentaire;
        // this.notes = notes;
        this.id = id
    }
}