# Projet Blog et React Blog

L'objectif de ce projet est de créé une API pour la base de donné avec node.js et express en Back, et affiche interface de blog en React et Redux en Front. Pour la sécurité en faisant une authentification avec hashing et JWT.

## description du projet
---
&nbsp;
## Choisir une thématique pour mon blog 

Comme j'aime bien la cusine du monde, donc j'ai choisi de faire un blog pour la cusine du monde.

### Use Case

## I Conception
---
&nbsp;
Diagramme de use-case: j'utilise StarUML pour réaliser mon diagramme de use-case, cela qui me permet de lister toutes les fonctionnalités  qui seront possibles sur mon blog

![Use-Case](diagrammes/usecase-blog.png)

Role = user (utilisateur inscrit)

Un user doit pouvoir s'inscrire, se connecter, consulter le site, et il peut aussi publier, ajouter, suprimer et update ses articles sur son espace personnel.

Role = lecteur (visiteur non inscrit)

N'importe quel lecteur qui peut consulter le site et lire les articles des autres users.


## II Use Story
---
&nbsp;

#### 1. *** user qui a un role d'utilisateur inscrit authentifié ***

- En tant que { visiteur du blog de site } rôle utilisateur
- je veux { pouvoir entrer dans mon espace privée, je dois enregistrer et lognin avec mpd} besoin action
- afin de { pouvoir trouver des réponses, publier et modifer des articles, ainsi donner des commentaires et des notes sur le blog } bénéfice valeur métier

Utilisateur inscrit, une fois il a inscrit sur le site, il peut login dans son espace personnel avec un mot de passer. il peut egalement publier, ajouter, suprimer et undate des articles dans son espace personnel, ainsi peut donner de commentaires et notes pour les autres utilisateurs.

> fonctionnalité : publier un article, ajouter, mis à jour et suprimer ses articles.

&nbsp;

#### 2. *** user qui a un role de lecteur non authentifié ***

- En tant que { visiteur du blog de site } rôle lecteur
- je veux { pouvoir chercher des informations sans m’inscrire } besoin action
- afin de { pouvoir trouver des réponses sur le blog } bénéfice valeur métier

> fonctionnalités : recherche des articles qui m'interésse avec un mot clé

&nbsp;

### Entities, Respository
---
&nbsp;

![Entity](diagrammes/entity-blog.png)

> Repository pour l'entity de post
- findAll()
- findByTitle()
- findByEmail()
- findByUserId()
- addPost()
- updatePost()
- deletePost()

## III Maquette : fonctionnelles de l'application
---
&nbsp;

![Maquette](diagrammes/maquette-blog.png)

**le lien backend vers déploiement Heroku**:
https://blogling.herokuapp.com/api/post

**le lien frontend vers déploiement Netlify**:
https://youthful-lewin-55b1fa.netlify.app/#/

**le lien vers depôt GITLAB**: 
https://gitlab.com/Ling69/projet-blog
https://gitlab.com/Ling69/blog-react
